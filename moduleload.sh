#!/bin/bash
# This file loads the dependencies for madupite on the ETH Zurich Euler cluster
# Make sure to run this on the new software stack (call env2lmod)
module load gcc/8.2.0 openmpi/4.1.4 python petsc/3.15.5 cmake hdf5 swig
