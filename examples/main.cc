// Example file to test installation
#include <cstdlib>
#include <cstring>
#include <experimental/filesystem>
#include <iostream>
#include <string>

#include "../include/mdp.hpp"

int main(int argc, char **argv) {
#if PETSC_VERSION_LT(3, 19, 0)
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
#else
  PetscInitialize(&argc, &argv, PETSC_NULLPTR, PETSC_NULLPTR);
#endif

  Mdp mdp;
  mdp.loadP("./transprob.bin");
  mdp.loadG("./stagecost.bin");
  mdp.generateInitCost("./examples/options.json");
  mdp.solve("./", "./examples/options.json");
  mdp.~Mdp();
  PetscFinalize();
  return 0;
}
