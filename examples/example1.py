import madupite as md

# every function call should be within the PETScContextManager
with md.PETScContextManager():
    # create an empty mdp instance
    mdp = md.PyMdp()
    # Since we use mpirun, every line of code will be executed by each rank.
    # Parts that should only be run by a single rank must be called inside
    # an if md.MPI_master() statement.
    if md.MPI_master():
        # this function creates a random mdp with 10 states, 8 actions,
        # sparsity 0.5, and uniformly distributed stagecost
        transprob, stagecost = md.generateArtificialMDP(8, 4, 0.5)
        # write the numpy/scipy matrices to PETSc binary format
        md.writePETScBinary(stagecost, "./stagecost.bin")
        md.writePETScBinary(transprob, "./transprob.bin")

    # load the transition probabilities and stage costs
    mdp.loadP("./transprob.bin")
    mdp.loadG("./stagecost.bin")

    # generate an initial cost and overwrite the solver options with the json file
    mdp.generateInitCost("./options.json")
    # as an example we investigate the impact of different discount factors
    for i in range(4):
        mdp["discount factor"] = (i + 1) * 0.99 / 4
        # The first argument specifies the folder where the output files will be stored
        # solve accepts a second argument which reads options from
        # a josn file that overwrite the existing options
        mdp.solve("./" + str(i) + "/")
    # Now let's try another inner solver
    mdp["solver"]["ksptype"] = "richardson"
    mdp["solver"]["preconditioner"] = "jacobi"
    # if you don't need an output file you can just assign the
    # empty string to cost filename, policy filename or metadata filename.
    mdp["cost filename"] = ""
    mdp.solve("./jac/")
