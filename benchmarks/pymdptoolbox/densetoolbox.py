from collections import defaultdict

import mdptoolbox as mdpt
import numpy as np
import pandas as pd


def bellmanResidual(P, G, discount, V=None, policy=None):
    if np.array(V is None).any():
        V = np.linalg.solve(
            np.eye(P.shape[1])
            - discount * np.swapaxes(P, 0, 1)[np.arange(len(policy)), policy],
            G[np.arange(len(policy)), policy],
        )
    return np.linalg.norm(
        V
        - np.max(discount * np.einsum("ikj,j->ik", np.swapaxes(P, 0, 1), V) + G, axis=1)
    )


def main():
    timings = defaultdict(list)
    residuals = defaultdict(list)
    runs = 10
    reps = 10
    for nstates in [2 ** (i + 3) for i in range(runs)]:
        pydata = np.load("./data/dense/pydata" + str(nstates) + ".npz")
        stagecostdense = pydata["stagecost"]
        transprobdense = pydata["transprob"]
        discount = 0.9999
        for solver, name in zip(
            [
                mdpt.mdp.PolicyIteration,
                mdpt.mdp.PolicyIterationModified,
                mdpt.mdp.ValueIteration,
            ],
            ["PI", "PIModified", "VI"],
        ):
            for i in range(reps):
                if name == "PIModified":
                    mdptoolbox = solver(
                        transprobdense, stagecostdense, discount, epsilon=1e-9
                    )
                elif name == "VI":
                    mdptoolbox = solver(
                        transprobdense, stagecostdense, discount, epsilon=1e-9
                    )
                elif name == "PI":
                    mdptoolbox = solver(transprobdense, stagecostdense, discount)
                mdptoolbox.run()
                timings[name].append(mdptoolbox.time)

                residuals[name].append(
                    bellmanResidual(
                        transprobdense,
                        stagecostdense,
                        discount,
                        policy=mdptoolbox.policy,
                    )
                )
    df = pd.DataFrame.from_dict(timings)
    df.to_csv("densetimingstoolbox.csv")
    df = pd.DataFrame.from_dict(residuals)
    df.to_csv("denseresidualstoolbox.csv")


if __name__ == "__main__":
    main()
