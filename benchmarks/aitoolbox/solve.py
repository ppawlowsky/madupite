# This file solves the tiger antelope example from AIToolbox,
# https://github.com/Svalorzen/AI-Toolbox/blob/master/examples/MDP/tiger_antelope.cpp
import madupite as md


def main():
    with md.PETScContextManager():
        mdp = md.PyMdp()
        for square_size in range(4, 15):
            mdp.loadP("./data/P" + str(square_size**4) + ".bin")
            mdp.loadG("./data/G" + str(square_size**4) + ".bin")
            mdp.generateInitCost("./options.json")
            for i in range(10):
                mdp.solve(str(square_size) + "/singlecore" + str(i))


if __name__ == "__main__":
    main()
