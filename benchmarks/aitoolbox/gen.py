import madupite as md

for square_size in range(4, 23):
    # function that maps a state(int) to coordinates of both animals
    def state2coords(state) -> list:
        return [
            (state % (square_size ** (i + 1))) // (square_size**i) for i in range(4)
        ]

    # function that maps coordinates of both animals to a state(int)
    def coords2state(coords):
        for i in range(4):
            if coords[i] == square_size:
                coords[i] = 0
            elif coords[i] == -1:
                coords[i] = square_size - 1
        state = 0
        for i in range(4):
            state += coords[i] * square_size ** (i)
        return state

    # 1d-distance of x and y in a wrap-around world
    def wrapDiff(x, y):
        diff1 = max(x, y) - min(x, y)
        diff2 = min(x, y) + square_size - max(x, y)
        return min(diff1, diff2)

    # manhattan distance in a wrap-around world
    def manhattanDistance(state, action=None):
        x_t, y_t, x_a, y_a = state2coords(state)
        # apply action
        if action is not None:
            if action == 3:  # right
                x_t = x_t + 1
            elif action == 2:  # left
                x_t = x_t - 1
            elif action == 0:  # up
                y_t = y_t + 1
            elif action == 1:  # down
                y_t = y_t - 1
        # measure distance
        return wrapDiff(x_t, x_a) + wrapDiff(y_t, y_a)

    # there are five actions: up,down,left,right,stand
    # the state space encodes the x and y position of the antelope and the tiger
    # the world has periodic boundary conditions
    def transprobfunc(state, action):
        coords = state2coords(state)
        x_t, y_t, x_a, y_a = coords
        idx = []
        val = []
        if x_t == x_a and y_t == y_a:
            # Final absorbing state, Tiger caught antelope
            idx = [state]
            val = [1]
        else:
            x_tn = x_t
            y_tn = y_t
            if action == 3:  # right
                x_tn = x_t + 1
            elif action == 2:  # left
                x_tn = x_t - 1
            elif action == 0:  # up
                y_tn = y_t + 1
            elif action == 1:  # down
                y_tn = y_t - 1

            idx = [
                coords2state([x_tn, y_tn, x_a, y_a]),
                coords2state([x_tn, y_tn, x_a + 1, y_a]),
                coords2state([x_tn, y_tn, x_a - 1, y_a]),
                coords2state([x_tn, y_tn, x_a, y_a + 1]),
                coords2state([x_tn, y_tn, x_a, y_a - 1]),
            ]

            # antelope does not move to tiger's position
            if manhattanDistance(state) <= 1:
                idx.remove(coords2state([x_tn, y_tn, x_t, y_t]))
                val = [1 / 4 for i in range(4)]
            else:
                val = [1 / 5 for i in range(5)]

        return idx, val

    def costfunc(state, action):
        coords = state2coords(state)
        x_t, y_t, x_a, y_a = coords
        if x_t == x_a and y_t == y_a:
            # Final absorbing state, Tiger caught antelope
            return -10
        elif action != 4:
            dist = manhattanDistance(state, action)
            if dist == 0:
                return -2.5
            elif dist == 1:
                return -2

        # # absorbing state modelling that allows discount factor 1
        # # instead of rewarding the final state we penalize every other state.
        # if x_t == x_a and y_t == y_a:
        #     return 0
        # else:
        #     return 10
        return 0

    nstates = square_size**4
    md.generateMDP(
        nstates,
        5,
        transprobfunc,
        costfunc,
        "./data/P" + str(nstates) + ".bin",
        "./data/G" + str(nstates) + ".bin",
    )
