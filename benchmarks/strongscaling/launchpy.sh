#!/bin/bash

jid=$(sbatch \
    --mail-type=BEGIN \
    --mail-user=ppawlowsky@ethz.ch \
    --output=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.out \
    --error=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.err \
    --time=0:30:00 \
    --nodes=1 \
    --ntasks=1\
    --ntasks-per-node=1 \
    --cpus-per-task=1\
    --cores-per-socket=1\
    --mem-per-cpu=128G \
    --wrap="mpirun python solve.py")
for i in {2..48}
do
   jid=$(sbatch \
   --output=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.out \
   --error=/cluster/home/ppawlowsky/bt-euler/data/slurm/%j/slurm.err \
   --time=0:15:00 \
   --nodes=$i\
   --cpus-per-task=1\
   --ntasks=$i\
   --dependency=afterany:${jid:20:8} \
   --ntasks-per-node=1 \
   --mem-per-cpu=$((128/$i))G \
   --wrap="mpirun python solve.py")
done
# specs from https://scicomp.ethz.ch/wiki/Euler#Euler_VIII
# per node:
# two 64 core cpu
# 512 GB RAM
# 920,618.0 MB Scratch

# student accounts can only use 128 GiB of RAM and 48 cores
