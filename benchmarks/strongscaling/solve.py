import os

import madupite as md

outdir = (
    "/cluster/home/ppawlowsky/bt-euler/data/strongscalingfinal2/"
    + str(os.getenv("SLURM_JOB_ID"))
    + "/"
)

with md.PETScContextManager():
    mdp = md.PyMdp()
    mdp.loadP("/cluster/scratch/ppawlowsky/mdp/transprob.bin")
    mdp.loadG("/cluster/scratch/ppawlowsky/mdp/stagecost.bin")
    mdp.generateInitCost(
        "/cluster/home/ppawlowsky/bt-euler/benchmarks/strongscaling/options.json"
    )
    for i in range(30):
        mdp.solve(outdir + str(i) + "/")
