import madupite as md
import numpy as np

nstates = 100000
high = int(nstates * 0.003)


def transprobfunc(state, action):
    idx = np.random.choice(nstates, size=np.random.randint(1, high=high), replace=False)
    idx = np.sort(idx)
    val = np.random.uniform(low=0.0, high=1.0, size=len(idx))
    val /= val.sum()
    return idx, val


def costfunc(state, action):
    return np.random.rand()


md.generateMDP(
    nstates,
    100,
    transprobfunc,
    costfunc,
    "./transprobsmall.bin",
    "./stagecostsmall.bin",
)
