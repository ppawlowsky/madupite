import argparse

import madupite as md


def main(ranks):
    outdir = (
        "/cluster/home/ppawlowsky/bt-euler/data/weakscalingfixonenode/"
        + str(ranks)
        + "/"
    )

    with md.PETScContextManager():
        mdp = md.PyMdp()
        mdp.loadP("/cluster/scratch/ppawlowsky/mdp/transprobws" + str(ranks) + ".bin")
        mdp.loadG("/cluster/scratch/ppawlowsky/mdp/stagecostws" + str(ranks) + ".bin")
        mdp.generateInitCost(
            "/cluster/home/ppawlowsky/bt-euler/benchmarks/weakscaling/options.json"
        )
        for i in range(30):
            mdp.solve(outdir + str(i) + "/")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--ranks",
        type=int,
        help="Number of ranks for weak scaling",
        required=True,
    )
    args = parser.parse_args()
    main(args.ranks)
