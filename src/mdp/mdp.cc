#include "../../include/mdp.hpp"

#include <cmath>
#include <ctime>
#include <fstream>
#include <iostream>

Mdp::Mdp() {
  // Initialize pointers to NULL
  P = NULL;
  G = NULL;
  P_policy = NULL;
  optcost = NULL;
  initcost = NULL;
  g_policy = NULL;
  nstates = 0;
  mactions = 0;

  // Get the rank and size of the MPI communicator
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  MPI_Comm_size(PETSC_COMM_WORLD, &size);
};

Mdp::~Mdp() {
  // Clear the data JSON object
  nlohmann::json().swap(data);

  // Destroy any allocated matrices and vectors
  if (P != NULL)
    MatDestroy(&P);
  if (G != NULL)
    MatDestroy(&G);
  if (P_policy != NULL)
    MatDestroy(&P_policy);
  if (optcost != NULL)
    VecDestroy(&optcost);
  if (initcost != NULL)
    VecDestroy(&initcost);
  if (g_policy != NULL)
    VecDestroy(&g_policy);
};

// Load the transition probability matrix from file
void Mdp::loadP(std::string filename) {
  // remove previously loaded matrix
  if (P != NULL)
    MatDestroy(&P);
  // Read and set sizes of mdp
  PetscViewer viewer;
  PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename.c_str(), FILE_MODE_READ, &viewer);
  PetscInt sizes[4];
  PetscInt dummy;
  // Read ClassId, Rows, Cols, NNZ
  PetscViewerBinaryRead(viewer, sizes, 4, &dummy, PETSC_INT);
  nstates = sizes[2];
  mactions = sizes[1] / sizes[2];
  localStates = PETSC_DECIDE;
  PetscSplitOwnership(PETSC_COMM_WORLD, &localStates, &nstates);
  PetscInt start = rank * (nstates / this->size) + std::min(nstates % size, rank);
  PetscInt *nnzperrow;
  PetscMalloc1(nstates * mactions, &nnzperrow);
  PetscViewerBinaryRead(viewer, nnzperrow, nstates * mactions, &dummy, PETSC_INT);
  maxnnzperrow = *nnzperrow;
  for (PetscInt i = start; i < start + localStates * mactions; i++)
    maxnnzperrow = std::max(nnzperrow[i], maxnnzperrow);
#if defined(PETSC_USE_64BIT_INDICES)
  MPI_Allreduce(MPI_IN_PLACE, &maxnnzperrow, 1, MPI_INT64_T, MPI_MAX, PETSC_COMM_WORLD);
#else
  MPI_Allreduce(MPI_IN_PLACE, &maxnnzperrow, 1, MPI_INT32_T, MPI_MAX, PETSC_COMM_WORLD);
#endif
  PetscFree(nnzperrow);
  PetscViewerDestroy(&viewer);
  // Reset to beginning of file and load matrix, force ownership splitting as a
  // multiple of mactions to ensure that data associated to one state resides on
  // one rank.
  PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename.c_str(), FILE_MODE_READ, &viewer);
  MatCreate(PETSC_COMM_WORLD, &P);
  MatSetSizes(P, localStates * mactions, PETSC_DECIDE, nstates * mactions, nstates);
  MatLoad(P, viewer);
  PetscViewerDestroy(&viewer);
};

// Load the stage cost matrix from file
void Mdp::loadG(std::string filename) {
  // remove previously loaded matrix
  if (G != NULL)
    MatDestroy(&G);
  // Read and set sizes of mdp
  PetscViewer viewer;
  PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename.c_str(), FILE_MODE_READ, &viewer);
  PetscInt sizes[3];
  PetscInt dummy;
  PetscViewerBinaryRead(viewer, sizes, 3, &dummy, PETSC_INT);
  nstates = sizes[1];
  mactions = sizes[2];
  localStates = PETSC_DECIDE;
  PetscSplitOwnership(PETSC_COMM_WORLD, &localStates, &nstates);
  PetscViewerDestroy(&viewer);

  // Reset to beginning of file and load matrix, force ownership splitting as
  // multiple of mactions to ensure that data associated to one state resides on
  // one rank.
  PetscViewerBinaryOpen(PETSC_COMM_WORLD, filename.c_str(), FILE_MODE_READ, &viewer);
  MatCreate(PETSC_COMM_WORLD, &G);
  MatSetSizes(G, localStates, PETSC_DECIDE, nstates, mactions);
  MatLoad(G, viewer);
  PetscViewerDestroy(&viewer);
};

// Generate the initial cost vector
void Mdp::generateInitCost(std::string options) {
  // If options are provided, read them from a file
  if (!options.empty()) {
    std::ifstream in(options);
    if (in.is_open()) {
      data = json::parse(in);
    } else {
      std::cout << "File could not be opened: " << options << "\n";
      return;
    }
  }

  if (nstates == 0 || mactions == 0) {
    PetscPrintf(PETSC_COMM_WORLD, "Can't generate init cost before stage-costs or transition "
                                  "probabilities have been loaded. \n");
  } else {
    MatCreateVecs(P, &initcost, PETSC_IGNORE);
    if (data.contains("initial cost type")) {
      if (data["initial cost type"].is_number()) {
        VecSet(initcost, data["initial cost type"]);
        return;
      }
      if (data["initial cost type"].is_string() && data["initial cost type"] == "random") {
        VecSetRandom(initcost, NULL);
        return;
      }
    }
    PetscPrintf(PETSC_COMM_WORLD, "Wrong or missing initial cost option.\n");
  }
}
// Compute the greedy policy based on the current cost-to-go estimate.
// This function determines the best action to take in each state to minimize the expected cost.
void Mdp::greedyPolicy(PetscInt *policy, PetscReal *residual, json *logjson) {
  Vec allCosts; // contains expected cost of each state-action pair
  Vec oldCost;  // contains previous cost estimate
  MatCreateVecs(P, &oldCost, &allCosts);
  VecCopy(optcost, oldCost);
  MatMult(P, optcost, allCosts);

  // Fill matrix C with the elements of allCosts
  PetscScalar *array;
  VecGetArray(allCosts, &array);

  Mat C;
  MatCreate(PETSC_COMM_WORLD, &C);
  MatSetType(C, MATMPIAIJ);
  PetscInt localActions;
  MatGetLocalSize(G, PETSC_IGNORE, &localActions);
  MatSetSizes(C, localStates, localActions, nstates, mactions);
  MatMPIAIJSetPreallocation(C, localActions, NULL, mactions - localActions, NULL);

  PetscInt start, end;
  VecGetOwnershipRange(allCosts, &start, &end);
  for (PetscInt i = start; i < end; i++) {
    MatSetValue(C, i / mactions, i % mactions, array[i - start], INSERT_VALUES);
  }
  MatAssemblyBegin(C, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(C, MAT_FINAL_ASSEMBLY);
  VecRestoreArray(allCosts, &array);
  VecDestroy(&allCosts);

  // C = G + discount*P*V
  MatAYPX(C, discount, G, UNKNOWN_NONZERO_PATTERN);

  // MatGetRowMin is only implemented for MPIAIJ and SEQDENSE
  // https://petsc.org/release/manualpages/Mat/MatGetRowMin/
  // It does not work for matrices that have more ranks than columns
  // Therefore, we use our own implementation
  MatGetOwnershipRange(C, &start, &end);
  PetscInt nnz;
  const PetscInt *col;
  const PetscScalar *vals;
  for (PetscInt i = 0; i < localStates; i++) {
    MatGetRow(C, i + start, &nnz, &col, &vals);
    const PetscReal *min = std::min_element(vals, vals + nnz);
    policy[i] = col[min - vals]; // pointer arithmetics
    // by storing the minimum of the greedy policy step in optcost
    // we get a free VI iteration that is a better initial guess for our
    // inner solver
    VecSetValue(optcost, i + start, *min, INSERT_VALUES);
    MatRestoreRow(C, i + start, &nnz, &col, &vals);
  }
  VecAssemblyBegin(optcost);
  VecAssemblyEnd(optcost);
  // currently: cost = min_{pi}(g^{pi} + discount*P^{pi}*V)
  // Now: Compute the infinity norm of the bellman residual
  // V - min_{pi}(g^{pi} + discount*P^{pi}*V)
  VecAYPX(oldCost, -1, optcost);
  VecNorm(oldCost, NORM_INFINITY, residual);
  VecDestroy(&oldCost);
  MatDestroy(&C);

  // logging
  if (rank == 0) {
    json obj = {{"time", MPI_Wtime() - solve_start_time},
                {"residual", *residual},
                {"inner solver", json::array()}};
    logjson->push_back(obj);
  }
}

// Extract the transition probability matrix for a given policy
void Mdp::extractTransProbMat(PetscInt *policy) {
  PetscInt start, end;
  MatGetOwnershipRange(P, &start, &end);
  PetscInt nnz;
  const PetscInt *cols;
  const PetscScalar *vals;
  // The shell version stores a nstates x nstates P^{policy}
  // The non-shell version stores I-\gamma P^{policy}
  // This requires another case distinction in solveCost
  if (useShell) {
    MatMPIAIJSetPreallocation(P_policy, maxnnzperrow, NULL, maxnnzperrow, NULL);
    for (PetscInt state = start / mactions; state * mactions < end; ++state) {
      PetscInt row_idx_P = state * mactions + policy[state - start / mactions];
      MatGetRow(P, row_idx_P, &nnz, &cols, &vals);
      MatSetValues(P_policy, 1, &state, nnz, cols, vals, INSERT_VALUES);
      MatRestoreRow(P, row_idx_P, &nnz, &cols, &vals);
    }
  } else {
    MatMPIAIJSetPreallocation(P_policy, maxnnzperrow + 1, NULL, maxnnzperrow + 1, NULL);
    PetscScalar *newVals = (PetscScalar *)malloc((maxnnzperrow + 1) * sizeof(PetscScalar));
    PetscInt *newCols = (PetscInt *)malloc((maxnnzperrow + 1) * sizeof(PetscInt));
    for (PetscInt state = start / mactions; state * mactions < end; ++state) {
      PetscInt row_idx_P = state * mactions + policy[state - start / mactions];
      MatGetRow(P, row_idx_P, &nnz, &cols, &vals);

      PetscBool foundDiagonal = PETSC_FALSE;

      for (PetscInt j = 0; j < nnz; j++) {
        newCols[j] = cols[j];
        newVals[j] = -discount * vals[j];
        if (cols[j] == state) {
          newVals[j] += 1;
          foundDiagonal = PETSC_TRUE;
        }
      }

      // If the diagonal element was not found in the cols array, add it
      if (!foundDiagonal) {
        newCols[nnz] = state;
        newVals[nnz] = 1;
        MatSetValues(P_policy, 1, &state, nnz + 1, newCols, newVals, INSERT_VALUES);
      } else {
        MatSetValues(P_policy, 1, &state, nnz, newCols, newVals, INSERT_VALUES);
      }
      MatRestoreRow(P, row_idx_P, &nnz, &cols, &vals);
    }
    free(newCols);
    free(newVals);
  }
  MatAssemblyBegin(P_policy, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(P_policy, MAT_FINAL_ASSEMBLY);
}

// Extract the cost vector for a given policy
void Mdp::extractCostVec(PetscInt *policy) {
  PetscInt start, end;
  MatGetOwnershipRange(G, &start, &end);
  for (PetscInt i = start; i < end; i++) {
    PetscScalar val;
    MatGetValue(G, i, policy[i - start], &val);
    VecSetValue(g_policy, i, val, INSERT_VALUES);
  }
  VecAssemblyBegin(g_policy);
  VecAssemblyEnd(g_policy);
}

// helper data structure that contains the test context
typedef struct {
  PetscScalar upperbound;
  PetscInt rank;
  json *logjson;
} iGMRESConvTestCtx;

// Convergence test from
// Inexact GMRES Policy Iteration for Large-Scale Markov Decision Processes
// https://arxiv.org/pdf/2211.04299.pdf
PetscErrorCode iGMRESConvTest(KSP ksp, PetscInt iteration, PetscReal residual,
                              KSPConvergedReason *reason, void *ctx) {
  // There is also access to the infinity-norm by requesting the solution vector
  // and then multiplying it with the shell
  // https://petsc.org/release/manualpages/KSP/KSPBuildSolution/
  // Vec solution;
  // KSPBuildSolution(ksp, NULL, &solution);
  // However, this is reasonably expensive and the l2-norm is an upper bound to
  // the infinity norm s.t. the criterion from the paper above is satisfied

  iGMRESConvTestCtx *testCtx = (iGMRESConvTestCtx *)ctx;
  if (residual <= testCtx->upperbound) {
    if (testCtx->rank == 0) {
      json obj = {{"iterations", iteration}, {"residual", residual}};
      testCtx->logjson->back()["inner solver"].push_back(obj);
    }
    *reason = KSP_CONVERGED_RTOL;
  }
  return 0;
}

// Matrix shell for I - \gamma P
typedef struct {
  Mat P_policy;
  PetscScalar discount;
} ShellCtx;

// Matrix Vector multiplication for Krylov solvers
void IdentityMinusDiscountP(Mat mat, Vec x, Vec y) {
  ShellCtx *shellCtx = NULL;
  MatShellGetContext(mat, (void **)&shellCtx);
  MatMult(shellCtx->P_policy, x, y);
  VecAYPX(y, -shellCtx->discount, x);
}

// Extract diagonal for jacobi preconditioning
void IdentityMinusDiscountPdiag(Mat mat, Vec x) {
  ShellCtx *shellCtx = NULL;
  MatShellGetContext(mat, (void **)&shellCtx);
  MatGetDiagonal(shellCtx->P_policy, x);
  VecScale(x, -shellCtx->discount);
  VecShift(x, 1);
}

// Solve for the optimal cost using the provided policy
void Mdp::solveCost(json *logjson, PetscReal *residual) {
  KSP ksp;
  std::string ksptype = data["solver"]["ksptype"].dump();
  KSPCreate(PETSC_COMM_WORLD, &ksp);
  KSPSetType(ksp, ksptype.substr(1, ksptype.size() - 2).c_str());
  // define matrix shell that represents (I-discount*P_policy)
  Mat shell;
  extern void IdentityMinusDiscountP(Mat, Vec, Vec);
  ShellCtx shellCtx;
  shellCtx.P_policy = P_policy;
  shellCtx.discount = discount;
  if (useShell) {
    MatCreateShell(PETSC_COMM_WORLD, localStates, localStates, nstates, nstates, &shellCtx, &shell);
    MatShellSetOperation(shell, MATOP_MULT, (void (*)(void))IdentityMinusDiscountP);
    // for jacobi preconditioning
    MatShellSetOperation(shell, MATOP_GET_DIAGONAL, (void (*)(void))IdentityMinusDiscountPdiag);
    KSPSetOperators(ksp, shell, shell);
  } else {
    // if we use the explicit storage format, P_policy already contains
    // I - \gamma P^\pi
    KSPSetOperators(ksp, P_policy, P_policy);
  }

  KSPSetNormType(ksp, KSP_NORM_UNPRECONDITIONED);
  iGMRESConvTestCtx testCtx;
  if (data["solver"]["inner stopping criterion"]["alpha"].is_number()) {
    testCtx.upperbound = (data["solver"]["inner stopping criterion"]["alpha"]).get<double>() *
                         ((1. - discount) / (1. + discount)) * (*residual);
    testCtx.logjson = logjson;
    testCtx.rank = rank;
    KSPSetConvergenceTest(ksp, iGMRESConvTest, &testCtx, NULL);
  } else if (data["solver"]["inner stopping criterion"]["alpha"] == "default") {
    testCtx.upperbound = ((1. - discount) / (1. + discount)) * (*residual);
    testCtx.logjson = logjson;
    testCtx.rank = rank;
    KSPSetConvergenceTest(ksp, iGMRESConvTest, &testCtx, NULL);
  } else if (data["solver"]["inner stopping criterion"]["alpha"] == "exact") {
    KSPSetTolerances(ksp, data["outer stopping criterion"], PETSC_DEFAULT, PETSC_DEFAULT,
                     PETSC_DEFAULT);
  }

  PC pc;
  if (data["solver"].contains("preconditioner")) {
    KSPGetPC(ksp, &pc);
    std::string pctype = data["solver"]["preconditioner"].dump();
    PCSetType(pc, pctype.substr(1, pctype.size() - 2).c_str());
  } else {
    PC pc;
    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCNONE);
  }
  PCSetFromOptions(pc);
  PCSetUp(pc);
  KSPSetFromOptions(ksp);
  KSPSetUp(ksp);
  KSPSetInitialGuessNonzero(ksp, PETSC_TRUE);
  KSPSolve(ksp, g_policy, optcost);
  KSPDestroy(&ksp);
  if (useShell) {
    MatDestroy(&shell);
  }
}

// solve MDP with iPI
double Mdp::solve(std::string outdir, std::string injson) {
  if (!injson.empty()) {
    std::ifstream in(injson);
    if (in.is_open()) {
      data = json::parse(in);
    } else {
      std::cout << "failed to open " << injson << "\n";
      return 0;
    }
  }
  if (data["solver"].contains("options")) {
    std::string options = data["solver"]["options"].dump();
    PetscOptionsInsertString(NULL, options.substr(1, options.size() - 2).c_str());
  }

  if (initcost == NULL) {
    generateInitCost();
  } else {
    PetscInt initcostsize;
    VecGetSize(initcost, &initcostsize);
    if (initcostsize != nstates) {
      generateInitCost();
    }
  }

  // Matrix shells do not support all preconditioners
  // The shell is only used for jacobi and none preconditioner
  if (data["solver"].contains("preconditioner")) {
    std::string pcstring = data["solver"]["preconditioner"].dump();
    if (pcstring.substr(1, pcstring.size() - 2) == "jacobi" ||
        pcstring.substr(1, pcstring.size() - 2) == "none") {
      useShell = PETSC_TRUE;
    } else {
      useShell = PETSC_FALSE;
    }
  }

  this->discount = data["discount factor"];
  outer_tolerance = data["outer stopping criterion"];
  json metadata = data;
  metadata += {"number of states", nstates};
  metadata += {"number of actions", mactions};
  metadata += {"ranks", size};
  std::time_t exec_time_date = std::time(NULL);
  metadata += {"time and date", std::ctime(&exec_time_date)};
  // store execution time together with residuals
  json logjson = json::array();
  PetscReal residual;

  MatCreate(PETSC_COMM_WORLD, &P_policy);
  MatSetSizes(P_policy, localStates, localStates, nstates, nstates);
  MatSetType(P_policy, MATMPIAIJ);
  MatCreateVecs(P, &g_policy, PETSC_IGNORE);

  PetscInt *policy;
  PetscMalloc1(localStates, &policy);
  VecDuplicate(initcost, &optcost);
  VecCopy(initcost, optcost);

  // Start of the solver
  solve_start_time = MPI_Wtime();
  greedyPolicy(policy, &residual, &logjson);
  while (residual > outer_tolerance) {
    extractTransProbMat(policy);
    extractCostVec(policy);
    solveCost(&logjson, &residual);
    greedyPolicy(policy, &residual, &logjson);
  }
  double solve_end_time = MPI_Wtime();
  MatDestroy(&P_policy);
  VecDestroy(&g_policy);

  // data logging
  if (data.contains("policy filename") && data["policy filename"].is_string()) {
    std::string filename =
        data["policy filename"].dump(0, ' ', false, json::error_handler_t::replace);
    filename.erase(std::remove(filename.begin(), filename.end(), '"'), filename.end());

    if (!filename.empty()) {
      FILE *file;

      PetscFOpen(PETSC_COMM_WORLD, (outdir + filename).c_str(), "w", &file);
      for (PetscInt i = 0; i < localStates; i++) {
        PetscSynchronizedFPrintf(PETSC_COMM_WORLD, file, "%i\n", policy[i]);
      }
      PetscSynchronizedFlush(PETSC_COMM_WORLD, file);
      PetscFClose(PETSC_COMM_WORLD, file);
    }
  }

  if (data.contains("cost filename") && data["cost filename"].is_string()) {
    std::string filename =
        data["cost filename"].dump(0, ' ', false, json::error_handler_t::replace);
    filename.erase(std::remove(filename.begin(), filename.end(), '"'), filename.end());
    if (!filename.empty()) {
      PetscViewer viewer;
      PetscViewerCreate(PETSC_COMM_WORLD, &viewer);
      PetscViewerSetType(viewer, PETSCVIEWERASCII);
      PetscViewerPushFormat(viewer, PETSC_VIEWER_ASCII_SYMMODU);
      PetscViewerFileSetMode(viewer, FILE_MODE_WRITE);

      PetscViewerFileSetName(viewer, (outdir + filename).c_str());
      VecView(optcost, viewer);
      PetscViewerDestroy(&viewer);
    }
  }

  if (data.contains("metadata filename") && data["metadata filename"].is_string()) {
    std::string filename =
        data["metadata filename"].dump(0, ' ', false, json::error_handler_t::replace);
    filename.erase(std::remove(filename.begin(), filename.end(), '"'), filename.end());
    if (!filename.empty()) {
      metadata["residual log"] = logjson;
      FILE *file;
      PetscFOpen(PETSC_COMM_WORLD, (outdir + filename).c_str(), "w", &file);
      PetscFPrintf(PETSC_COMM_WORLD, file,
                   (metadata.dump(2, ' ', false, json::error_handler_t::replace)).c_str());
      PetscFClose(PETSC_COMM_WORLD, file);
    }
  }
  PetscFree(policy);
  return solve_end_time - solve_start_time;
}
