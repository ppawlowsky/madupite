Welcome to madupite's documentation!
======================================
``madupite`` is a python package for solving large-scale **Ma**\ rkov decision processes (MDP) using **d**\ istrib\ **u**\ ted inexact **p**\ olicy **ite**\ ration `(iPI) <https://arxiv.org/pdf/2211.04299>`_. The library is written in C++ and uses distributed sparse linear solvers from `PETSc <https://petsc.org/>`_.


.. toctree::
   :maxdepth: 2

   Installation <install>
   Tutorial <tutorial>
   Benchmarks <benchmarks>
   API Reference <apiref>


Index
==================

* :ref:`genindex`

.. :doc:`Examples<examples>`
